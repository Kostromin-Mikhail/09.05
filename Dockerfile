FROM centos:7
RUN yum update -y 
RUN yum install -y centos-release-scl
RUN yum install -y rh-python38
RUN yum install -y python3-pip
RUN pip3 install flask flask_restful flask_jsonpify
ADD python-api.py /python_api
ENTRYPOINT ["python3", "/python_api/python-api.py"]
